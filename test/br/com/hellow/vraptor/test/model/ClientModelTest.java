package br.com.hellow.vraptor.test.model;

import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import br.com.hellow.vraptor.model.Client;

public class ClientModelTest implements ICRUDTest{
	
	Datastore datastore = null;
	
	@Before
	public void setup(){
		MongoClient mongoClient = new MongoClient();
		datastore = new Morphia().createDatastore(mongoClient, "testClient");
	}

	public Client setObjectClient(){
		
		Client client = new Client();
		client.setName("Am�rico");
		client.setAge(26);
		client.setAdress("Av. 9 n�mero 213. Residencial Uni�o.");
		return client;
	}
	
	@Test
	public void save(){
		Client client = setObjectClient();
		datastore.save(client);
		System.out.println("Successfully Added Customer !");
	}


	@Test
	public void update() {
		
	}

	@Test
	public void remove() {
		
	}

	@Override
	public void findAll() {
		
	}
	
	@Override
	public void findById() {
		
	}

	@Override
	public void delete() {
		
	}
}
