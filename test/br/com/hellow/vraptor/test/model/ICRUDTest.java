package br.com.hellow.vraptor.test.model;

public interface ICRUDTest {
	
	public void save();			// Create
	public void findAll();		// Read
	public void findById();
	public void update(); 		// Update
	public void delete(); 		// Delete
}
