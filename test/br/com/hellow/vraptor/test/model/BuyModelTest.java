package br.com.hellow.vraptor.test.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.google.gson.Gson;
import com.mongodb.MongoClient;

import br.com.hellow.vraptor.model.Buy;
import br.com.hellow.vraptor.model.Client;
import br.com.hellow.vraptor.model.Product;

public class BuyModelTest implements ICRUDTest{
	
	Datastore datastore = null;
	
	@Before
	public void setup(){
		MongoClient mongoClient = new MongoClient();
		datastore = new Morphia().createDatastore(mongoClient, "testClient");
	}
	
	private Buy getPopulatedObject() {
		
		Product snakers = new Product();
		snakers.setNameProduct("Nike");
		snakers.setValueProduct(50000);
		
		Product glasses = new Product();
		glasses.setNameProduct("Ray Ban");
		glasses.setValueProduct(450000);
		
		List<Product> products = new ArrayList<>();
		products.add(glasses);
		products.add(snakers);
		
		datastore.save(snakers);
		datastore.save(glasses);
		System.out.println("\nProduct Successfully Added !");
		
		Client client = new Client();
		client.setName("Rafael Am�rico");
		client.setAge(23);
		client.setAdress("Rua 2654");
		datastore.save(client);
		System.out.println("\nCustummer Successfully Added !");

		
		Buy b = new Buy();
		b.setClient(client);
		b.setValueBuy(950000);
		b.setProducts(products);
		
		return b;
	}
	
	@Test
	public void testJson() {
		
		Product snakers = new Product();
		snakers.setNameProduct("Nike");
		snakers.setValueProduct(50000);
		
		Product glasses = new Product();
		glasses.setNameProduct("Ray Ban");
		glasses.setValueProduct(450000);
		
		List<Product> products = new ArrayList<>();
		products.add(glasses);
		products.add(snakers);
		
		datastore.save(snakers);
		datastore.save(glasses);
		System.out.println("\nProduct Successfully Added !");
		
		Client client = new Client();
		client.setName("Rafael Am�rico");
		client.setAge(23);
		client.setAdress("Rua 2654");
		datastore.save(client);
		System.out.println("\nCustummer Successfully Added !");
		
		
		Buy b = new Buy();
		b.setClient(client);
		b.setValueBuy(950000);
		b.setProducts(products);
		
		Gson gson = new Gson();
		
		System.out.println(gson.toJson(b));
		
	}
	
	@Test
	public void save() {
		
		Buy b = getPopulatedObject();
		
		datastore.save(b);
		System.out.println("\nBuy Successfully Added !");
	}

	@Test
	public void update() {
		
		String id = "56fa9ddd2aa12f0f6cb58fe6";
		
		Query<Buy> qAntes = datastore.find(Buy.class, "_id", new ObjectId(id));
		System.out.println("Valor Antes: " + qAntes.get().getValueBuy());
		
		UpdateOperations<Buy> ops = datastore.createUpdateOperations(Buy.class);
		ops.set("value", "85000");
		datastore.update(qAntes, ops);
		
		Query<Buy> qDepois = datastore.find(Buy.class, "_id", new ObjectId(id));
		System.out.println("Valor Depois: " + qDepois.get().getValueBuy());
	}
	
	@Test
	public void findById() {

		String _id = "56fa9ddd2aa12f0f6cb58fe6";
		ObjectId id = new ObjectId(_id);
		
		Buy buy = (Buy) datastore.get(Buy.class, id);
		System.out.println("id: " 			+ buy.getId());
		System.out.println("Client: " 		+ buy.getClient().getName());
		System.out.println("Products: " 	+ buy.getProducts().size());
		System.out.println("Total value: " 	+ buy.getValueBuy());
	}
	
	@Test
	public void findAll() {
		
		Query<Buy> qBuy = datastore.find(Buy.class);
		
		List<Buy> buys = qBuy.asList();
		for (Buy b : buys) {
			System.out.println("id: " 			+ b.getId());
			System.out.println("Client: " 		+ b.getClient().getName());
			System.out.println("Products: " 	+ b.getProducts().size());
			System.out.println("Total value: " 	+ b.getValueBuy() + "\n-----");
		}
		
		System.out.println("\nSearch was Successful!");
	}

	@Test
	public void delete() {
		ObjectId id = new ObjectId("56facc9b2aa12f1738c3fca8");
		datastore.delete(Buy.class, id);
		System.out.println("Buy deleted Successful!");
	}
}
