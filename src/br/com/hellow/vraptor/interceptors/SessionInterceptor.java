package br.com.hellow.vraptor.interceptors;

import javax.enterprise.context.RequestScoped;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.hellow.vraptor.interceptors.annotation.SessionIAnnotation;

@Intercepts
@RequestScoped
public class SessionInterceptor {
	
	boolean flag = true;
	
	@Accepts
	public boolean accepts( ControllerMethod method ){
		return method.containsAnnotation(SessionIAnnotation.class);
	}
	
	@AroundCall
	public void intercept( SimpleInterceptorStack stack ) {
		
		System.out.println("\n\tIntercepting... Session\n");
		if ( flag == true ) {
			stack.next();
		}
	}
}
