package br.com.hellow.vraptor.interceptors;

import javax.enterprise.context.RequestScoped;

import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.interceptor.AcceptsWithAnnotations;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.hellow.vraptor.interceptors.annotation.HellowIAnnotation;

@Intercepts( after = LoginInterceptor.class )
@RequestScoped
@AcceptsWithAnnotations(HellowIAnnotation.class)
public class HellowInterceptor {
	
	private boolean flag = true;
	
	@AroundCall
	public void intercept( SimpleInterceptorStack stack ) {
		
		System.out.println("\n\tIntercepting... Hellow VRaptor Marvado !\n");
		if ( flag == true ) {
			stack.next();
		}
	}
}
