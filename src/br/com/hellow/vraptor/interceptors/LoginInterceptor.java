package br.com.hellow.vraptor.interceptors;

import javax.enterprise.context.RequestScoped;

import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.interceptor.AcceptsWithAnnotations;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.hellow.vraptor.interceptors.annotation.HellowIAnnotation;
import br.com.hellow.vraptor.interceptors.annotation.LoginIAnnotation;

@Intercepts( before = HellowInterceptor.class )
@RequestScoped
@AcceptsWithAnnotations({LoginIAnnotation.class, HellowIAnnotation.class})
public class LoginInterceptor {
	
	private boolean flag = true;
	
	@AroundCall
	public void intercept( SimpleInterceptorStack stack ) {
		
		System.out.println("\n\tIntercepting... Login\n");
		if (flag == true) {
			stack.next();
		}
	}
}
