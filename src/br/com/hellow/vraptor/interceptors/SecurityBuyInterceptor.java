package br.com.hellow.vraptor.interceptors;

import javax.enterprise.context.RequestScoped;

import br.com.caelum.vraptor.AfterCall;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.BeforeCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;

@Intercepts
@RequestScoped
public class SecurityBuyInterceptor {
	
	@BeforeCall
	public void interceptBefore(){
		System.out.println("\n> Running Security - Before\n");
	}
	
	@AroundCall
	public void intercept( SimpleInterceptorStack stack ) {
		
		System.out.println("\n\tIntercepting... Security Buy\n");
		stack.next();
	}
	
	@AfterCall
	public void interceptAfter() {
		System.out.println("\n> Running Security - After\n");
	}
}
