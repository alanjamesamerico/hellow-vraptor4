package br.com.hellow.vraptor.dao.base;

import java.util.List;

import org.mongodb.morphia.Key;

import br.com.hellow.vraptor.model.base.IEntity;
import br.com.hellow.vraptor.util.MongoDBUtil;

public abstract class AbstractDao < T extends IEntity > {
	
	public Key<T> save( T entity ) {
		return MongoDBUtil.getDataStore().save(entity);
	}
	
	public void saveList( List<T> list ) {
		for (T entity : list) {
			MongoDBUtil.getDataStore().save(entity);
		}
	}
	
	public void delete( T entity ) {
		MongoDBUtil.getDataStore().delete(entity);
	}
	
}
