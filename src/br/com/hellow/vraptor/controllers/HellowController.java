package br.com.hellow.vraptor.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.hellow.vraptor.interceptors.annotation.HellowIAnnotation;

@Controller
@Path("/hellow")
public class HellowController {
	
	@Inject private Result result; 
	
	@Get("/home")
	@HellowIAnnotation
	public void home() {
//		result.include("mensagem","Hellow VRaptor Marvado");
		String response = "Hellow VRaptor Marvado";
		result.use(Results.json()).withoutRoot().from(response).serialize();
	}
	
	@Get("/say/{say}")
	@Consumes("application/json")
	public void sayHellow(String say) {
		result.use(Results.json()).withoutRoot().from(say).serialize();
	}
	
	@Post("/say/goodBye")
	@Consumes("application/json")
	@HellowIAnnotation
	public void sayGoodBye(String say) {
		result.use(Results.json()).withoutRoot().from(say).serialize();
	}
}
