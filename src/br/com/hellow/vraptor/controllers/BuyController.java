package br.com.hellow.vraptor.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.hellow.vraptor.bo.BuyBO;
import br.com.hellow.vraptor.interceptors.annotation.SecurityBuyIAnotation;
import br.com.hellow.vraptor.model.Client;
import br.com.hellow.vraptor.model.Product;

@Controller
@Path("/buy")
public class BuyController {
	
	@Inject
	private BuyBO buyBO;
	
	@Inject
	private Result result;
	
	@Post("/checkout")
	@Consumes("application/json")
	@SecurityBuyIAnotation
	public void checkout(Client client, List<Product> products ){
		
		String resultPayment = buyBO.makeBuyOfClient(client, products);
		result.use(Results.json()).withoutRoot().from(resultPayment).serialize();
	}
	
	@Get("/view")
	public void viewBuy(){
		
	}
}
