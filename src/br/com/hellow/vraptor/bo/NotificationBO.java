package br.com.hellow.vraptor.bo;

import java.time.Instant;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import br.com.hellow.vraptor.model.Buy;

@ApplicationScoped
public class NotificationBO {
	
	public void notify( @Observes Buy buy ) {
		System.out.println("\nNotification \t[" + Instant.now() + "] - System Notified\n");
	}
}
