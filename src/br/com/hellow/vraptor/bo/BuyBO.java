package br.com.hellow.vraptor.bo;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import br.com.hellow.vraptor.dao.buy.BuyDao;
import br.com.hellow.vraptor.dao.client.ClientDao;
import br.com.hellow.vraptor.dao.product.ProductDao;
import br.com.hellow.vraptor.model.Buy;
import br.com.hellow.vraptor.model.Client;
import br.com.hellow.vraptor.model.Product;

@ApplicationScoped
public class BuyBO {
	
	@Inject
	private Event<Buy> eventBuy;
	@Inject
	private BuyDao buyDao;
	@Inject
	private ProductDao productDao;
	@Inject
	private ClientDao clientDao;
	
	public String makeBuyOfClient(Client client, List<Product> products){
		
		int priceBuy = calculatePurchasePrice(products);
		
		Buy buy = new Buy(client, products, priceBuy);
		
		clientDao.save(client);
		productDao.saveList(products);
		buyDao.save(buy);
		// Fire Event
		eventBuy.fire(buy);
		
		return "Buy completed successfully!";
	}
	
	public int calculatePurchasePrice(List<Product> products){
		
		int priceTotal = 0;
		for (Product product : products) {
			priceTotal = priceTotal + product.getValueProduct();
		}
		
		return priceTotal;
	}
}
