package br.com.hellow.vraptor.bo;

import java.time.Instant;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import br.com.hellow.vraptor.model.Buy;

@ApplicationScoped
public class PaymentBO {
	
	public void makePayment( @Observes Buy buy ){
		System.out.println("\nPayment \t[" + Instant.now() + "] - Payment Made\n");
	}
}
