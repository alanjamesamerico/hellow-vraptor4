package br.com.hellow.vraptor.bo;

import java.time.Instant;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import br.com.hellow.vraptor.model.Buy;

@ApplicationScoped
public class StockBO {
	
	public void giveLowInStock( @Observes Buy buy ) {
		System.out.println("\nStock \t\t[" + Instant.now() + "] - Low Held\n");
	}
}
