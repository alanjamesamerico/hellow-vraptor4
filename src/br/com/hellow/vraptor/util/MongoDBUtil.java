package br.com.hellow.vraptor.util;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class MongoDBUtil {
	
	private final static MongoClient _MC = new MongoClient();
	
	public static Datastore getDataStore() {
		Datastore ds = new Morphia().createDatastore(_MC, "hellowvraptor4"); 
		return ds;
	}
	
	public static Datastore getDataStore(String dataBase) throws Exception {
		
		Datastore ds;
		
		try {
			ds = new Morphia().createDatastore(_MC, dataBase);
		} catch (Exception e) {
			throw new Exception("Invalide Collection");
		}
		
		return ds;
	}
}
