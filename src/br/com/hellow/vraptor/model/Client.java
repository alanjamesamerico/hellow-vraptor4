package br.com.hellow.vraptor.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

import br.com.hellow.vraptor.model.base.AbstractEntity;
import br.com.hellow.vraptor.model.base.IEntity;

@Entity("client")
public class Client extends AbstractEntity implements Serializable, IEntity {
	
	private static final long serialVersionUID = 6164121541939745695L;

	@Property
	private String name;
	
	@Property
	private int age;
	
	@Property
	private String adress;
	
	public Client() {
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
}
