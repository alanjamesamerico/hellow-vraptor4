package br.com.hellow.vraptor.model.base;

import org.bson.types.ObjectId;

public interface IEntity {
	
    public ObjectId getId();

    public void setId(ObjectId id);
}
