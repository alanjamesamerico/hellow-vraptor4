package br.com.hellow.vraptor.model.base;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;

public abstract class AbstractEntity implements IEntity {
	
	@Id
	private ObjectId id;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}
}
