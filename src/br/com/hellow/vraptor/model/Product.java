package br.com.hellow.vraptor.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

import br.com.hellow.vraptor.model.base.AbstractEntity;
import br.com.hellow.vraptor.model.base.IEntity;

@Entity("product")
public class Product extends AbstractEntity implements Serializable, IEntity {
	
	private static final long serialVersionUID = 5997535205287628896L;

	@Property("name")
	private String nameProduct;
	
	@Property("value")
	private int valueProduct;
	
	public Product() {
	}
	
	public String getNameProduct() {
		return nameProduct;
	}
	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}
	public int getValueProduct() {
		return valueProduct;
	}
	public void setValueProduct(int valueProduct) {
		this.valueProduct = valueProduct;
	}
}
