package br.com.hellow.vraptor.model;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Reference;

import br.com.hellow.vraptor.model.base.AbstractEntity;
import br.com.hellow.vraptor.model.base.IEntity;

@Entity("buy")
public class Buy extends AbstractEntity implements Serializable, IEntity {
	
	private static final long serialVersionUID = 7022464733429002864L;

	@Property("price")
	private int priceBuy;
	
	@Reference("costummer")
	private Client client;
	
	@Reference("items")
	private List<Product> products;
	
	public Buy(){
	}
	
	public Buy( Client client, List<Product> products, int priceBuy ) {
		this.client	= client;
		this.products = products;
		this.priceBuy = priceBuy;
	}
	
	public int getValueBuy() {
		return priceBuy;
	}

	public void setValueBuy(int valueBuy) {
		this.priceBuy = valueBuy;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
